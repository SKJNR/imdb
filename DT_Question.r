## Wrute

## Decision Trees

###Load libraries
library(rpart)       ## to create the tree
library(rpart.plot)  ## to plot the tree
library(janitor)  ## for snake case
library(caTools)  ## for testing/training split


##  Q1. Call in data from "employee.csv" with a header


## Q2 Clean names with the clean_name function and convert all names into lower case.


## Q3 change 7 variables that should be factor variables to as.factor



## Q4  Create training and testing sets with splitRatio=.50: the data set will be named as "train" and "test"  :
set.seed(20)  ###  making sure this is reproducible




## Q5  fitting a decision tree specifying that the smallest node will contain at least 50 obs 
with rpart function return the results to "fit". and print it.



###  Q6. prune tree with rpart.plot function



### Q7  Make predictions and save it to "p" and 
 


### Q8 what are the class of p.

### Q9 print the size of matrix


### Q10 Convert p into a data frame and what is the class of the data frame.



### Q11  Create a table that includes the probabilites with the test data set.



### Q12  Sort by probability so we can see what the higher and lower probabilites look like in terms
### of employee behavior



### Q13  classify the prediction with the cutoff value, 0.5 


names(p)
summary(p$attrit_prediction)


### Q14 Create a table of predictions vs. actuals:


### Q15 classify the prediction (round 2) with the cutoff, .2



summary(p$attrit_prediction2)
### Create a table of predictions vs. actuals:
table(test$attrition, p$attrit_prediction2)

